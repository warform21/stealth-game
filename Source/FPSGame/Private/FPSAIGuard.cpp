// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSAIGuard.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "FPSGameMode.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AFPSAIGuard::AFPSAIGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	SetGuardState(EAIState::Idle);


}

// Called when the game starts or when spawned
void AFPSAIGuard::BeginPlay()
{
	Super::BeginPlay();
	
	PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPSAIGuard::OnPawnSeen);

	PawnSensingComp->OnHearNoise.AddDynamic(this, &AFPSAIGuard::OnNoiseHeard);
		
	AGameModeBase* gameModeBase = /*world->GetAuthGameMode()*/UGameplayStatics::GetGameMode(this);

	if (gameModeBase != nullptr)
	{
		gm = Cast<AFPSGameMode>(gameModeBase);

		gm->GuardReaction.AddDynamic(this, &AFPSAIGuard::GuardLost);

		OriginalRotation = GetActorRotation();

		if (bPatrol)
		{
			MoveToNextPatrolPoint();
		}
	}
}

void AFPSAIGuard::OnPawnSeen(APawn *SeenPawn)
{
	if (SeenPawn == nullptr)
	{
		return;
	}

	//DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 32.0f, 12, FColor::Red, false, 10.0f);
	SetGuardState(EAIState::Alerted);

	
	if (gm)
	{
		gm->CompleteMission(SeenPawn, false);
	}

}

void AFPSAIGuard::OnNoiseHeard(APawn* NoiseInstigator, const FVector& Location, float Volume)
{
	if (GuardState == EAIState::Alerted)
	{
		return;
	}
	//DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Green, false, 10.0f);

	FVector Direction = Location - GetActorLocation();
	Direction.Normalize();

	FRotator NewLookAt =FRotationMatrix::MakeFromX(Direction).Rotator();
	NewLookAt.Pitch = 0.0f;
	NewLookAt.Roll = 0.0f;
	SetActorRotation(NewLookAt);
	SetGuardState(EAIState::Suspicious);

	GetWorldTimerManager().ClearTimer(GuardAlertedTimerHandle);
	GetWorldTimerManager().SetTimer(GuardAlertedTimerHandle, this, &AFPSAIGuard::OnTimerExpired, 3.0f, false);

	GuardStopMovement();
}
void AFPSAIGuard::GuardLost()
{
	if (GuardState == EAIState::Suspicious)
	{
		GuardState = EAIState::Idle;
	}
	GuardStopMovement();
	GetWorldTimerManager().ClearTimer(GuardAlertedTimerHandle);
}

void AFPSAIGuard::GuardStopMovement()
{
	AController* GuardController = this->GetController();
	if (GuardController)
	{
		GuardController->StopMovement();
	}
}

void AFPSAIGuard::OnTimerExpired()
{
	if (GuardState == EAIState::Alerted)
	{
		return;
	}
	SetActorRotation(OriginalRotation);
	SetGuardState(EAIState::Idle);

	if (bPatrol)
	{
		MoveToNextPatrolPoint();
	}
}


void AFPSAIGuard::OnRep_GuardState()
{
	OnStateChanged(GuardState);
}

void AFPSAIGuard::SetGuardState(EAIState NewState)
{
	if (GuardState == NewState)
	{
		return;
	}
	GuardState = NewState;

	OnRep_GuardState();
}


// Called every frame
void AFPSAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentPatrolPoint)
	{
		FVector Distance = GetActorLocation() - CurrentPatrolPoint->GetActorLocation();
		if (Distance.Size() < 50 )//&& bPatrol)
		{
			MoveToNextPatrolPoint();
		}
	}

}

void AFPSAIGuard::MoveToNextPatrolPoint()
{
	if (CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
	{
		CurrentPatrolPoint = FirstPatrolPoint;
	}
	else 
	{
		CurrentPatrolPoint = SecondPatrolPoint;
	}

	UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
}

void AFPSAIGuard::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSAIGuard, GuardState);
}

