// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "DelegateCombinations.h"
#include "GameFramework/GameModeBase.h"
#include "FPSGameMode.generated.h"

class AFPSAIGuard;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMissionCompleteGuardReact);

UCLASS()
class AFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
	

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Spectating")
	TSubclassOf<AActor> SpectatingViewpointClass;

public:

	AFPSGameMode();


	FMissionCompleteGuardReact GuardReaction;

	void CompleteMission(APawn *InstigatorPawn, bool bMissionSucces);

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void OnMissionComplete(APawn* InstigatorPawn, bool bMissionSucces);
};



